Ext.define('AuthCodeMapping.model.AuthCode', {
    extend: 'Ext.data.Model',
    alias: 'model.deviceMap',

    requires: [
        'Ext.data.Field',
    ],

    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'used',
            type: 'auto'
        },
        {
            name: 'available',
            type: 'auto'
        },{
            name: 'total',
            type: 'auto'
        }
    ]
});
