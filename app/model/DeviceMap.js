Ext.define('AuthCodeMapping.model.DeviceMap', {
    extend: 'Ext.data.Model',
    alias: 'model.deviceMap',

    requires: [
        'Ext.data.Field',
    ],

    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'name',
            type: 'auto'
        },
        {
            name: 'F1',
            type: 'auto'
        },
        {
            name: 'F2',
            type: 'auto'
        },{
            name: 'F3',
            type: 'auto'
        },{
            name: 'F4',
            type: 'auto'
        },{
            name: 'F5',
            type: 'auto'
        },{
            name: 'F6',
            type: 'auto'
        },
    ]
});
