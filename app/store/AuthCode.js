Ext.define('AuthCodeMapping.store.AuthCode', {
    extend: 'Ext.data.Store',
    alias: 'store.authCodes',

    requires: [
        'AuthCodeMapping.model.DeviceMap',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            model: 'AuthCodeMapping.model.AuthCode',
            storeId: 'AuthCodes',
            proxy: {
                type: 'ajax',
                url : 'json/authCodes.json',
                reader: {
                    type: 'json',
                    idProperty: 'id',
                    root: 'contents.content',
                    totalProperty: 'contents["@total"]'
                }
            }
        }, cfg)]);
    }
});
