Ext.define('AuthCodeMapping.store.DeviceMap', {
    extend: 'Ext.data.Store',
    alias: 'store.deviceMaps',

    requires: [
        'AuthCodeMapping.model.DeviceMap',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            model: 'AuthCodeMapping.model.DeviceMap',
            storeId: 'DeviceMaps',
            proxy: {
                type: 'ajax',
                url : 'json/deviceMap.json',
                reader: {
                    type: 'json',
                    idProperty: 'id',
                    root: 'contents.content',
                    totalProperty: 'contents["@total"]'
                }
            }
        }, cfg)]);
    }
});
