# ext-theme-neptune-81c98e99-53e4-455a-9ea6-ffb24c46faac/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-81c98e99-53e4-455a-9ea6-ffb24c46faac/sass/etc"`, these files
need to be used explicitly.
